import { React, useState } from "react";
import "./App.css";
import ProPage from "./components/Promotion/ProPage";
import ReservationPage from "./components/Reservation/ReservationPage";
import ButtonToTop from "./components/buttonToTop/ButtonToTop";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Header from "./components/header/Header";
import Main from "./components/mainpage/Main";
import Footer from "./components/footer/Footer";

function App() {
  const [showReserve, setShowReserve] = useState(false);
  const handleShowReserve = () => {
    setShowReserve(!showReserve);
  };

  const handleNoti = (event) => {
    event.preventDefault();
    handleShowReserve();
    setTimeout(() => {
      toast.success("Booking Successful!", {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: "dark",
      });
    }, 500);
  };

  return (
    <div className="bigContainer">
      <Header showReversePage={handleShowReserve}></Header>
      <Main></Main>
      <Footer></Footer>
      <ProPage />
      <ReservationPage
        showPage={showReserve}
        closePage={handleShowReserve}
        notification={handleNoti}
      />
      <ButtonToTop />
      <ToastContainer />
    </div>
  );
}

export default App;
