import React from "react";
import textSplit from "../../function/textSplit";
import "./Main.css";
import ScrollEvent from "../../function/ScrollEvent";

const Main = () => {
  return (
    <div>
      <div className="bg-container"></div>
      <div className="about">
        <span className="about-title">Our Story</span>
        <div className="about-img"></div>
        <div className="about-des">
          SONA - where the quintessence of multi-sensory cuisine resonates in
          the space with a view of the West Lake from above. Immerse yourself in
          the liberal interference of culinary art and the natural picture of
          West Lake, enjoy the wonderful feeling of delicate dishes and drinks
          in the heart of Hanoi.
        </div>
        {/* <button>See More</button> */}
      </div>

      <div className="menu">
        <img
          className="menu-img"
          src="https://images.getbento.com/accounts/65e1fe01d2241d86184cbee4f6645488/media/images/21908REIGN_RESTAURANT_RICK_OBRIEN_MR22-81.jpg?w=1200&fit=max&auto=compress,format"
          alt="food"
        ></img>

        <div className="menu-des">
          <div className={`menu-title ${ScrollEvent(0.9) ? "spin-char" : ""} `}>
            {textSplit("Inspiration", "char")}
          </div>
          <div className={`menu-text ${ScrollEvent(0.9) ? "spin-word" : ""}`}>
            {textSplit(
              "Our creative, food and beverage program combines satisfying staples with imaginative twists. From boozy drag show brunches, classic Americana breakfasts, special occasions and everything in between, SONA has something for everyone...",
              "word"
            )}
          </div>
          <button className={`menu-btn ${ScrollEvent(0.9) ? "spin-360" : ""}`}>
            See Our Menu
          </button>
        </div>
      </div>

      <div className="menu">
        <div className="menu-des">
          <div className={`menu-title ${ScrollEvent(2) ? "spin-char" : ""}`}>
            {textSplit("Happenings", "char")}
          </div>
          <div className={`menu-text ${ScrollEvent(2) ? "spin-word" : ""}`}>
            {textSplit(
              "From seasonal menus to Holiday Celebrations, find out what's going on at SONA!",
              "word"
            )}
          </div>
          <button className={`menu-btn ${ScrollEvent(2) ? "spin-360" : ""}`}>
            View Happenings
          </button>
        </div>
        <img
          className="menu-img"
          src="https://resizer.otstatic.com/v2/photos/wide-huge/1/24743311.jpg"
          alt="food"
        ></img>
      </div>
      <div className="map">
        <div className={`map-info ${ScrollEvent(3) ? "spin-360" : ""}`}>
          <div className="map-name">SONA</div>
          <div className="map-des">Xuan Dieu, Tay Ho, Viet Nam</div>
        </div>
      </div>
    </div>
  );
};

export default Main;
