import React, { useState } from "react";
import "./Banner.css";
import { bannerData } from "../data/menudata.js";

const Banner = () => {
  const [currentSlide, setcurrentSlide] = useState(0);
  const slideLenght = bannerData.length;

  const nextSlide = () => {
    setcurrentSlide(currentSlide === slideLenght - 1 ? 0 : currentSlide + 1);
  };

  const auto = true;
  let slideInterval;
  let slideIntervalTime = 6000;

  function autoSlide() {
    slideInterval = setInterval(nextSlide, slideIntervalTime);
  }

  return (
    <div className="banner">
      {bannerData.map((slide, index) => {
        return <img src={slide.img} alt="no" className="pic" />;
      })}
    </div>
  );
};

export default Banner;
