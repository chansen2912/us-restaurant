import React from "react";
import "./Menuchoice.css";

const Menuchoice = () => {
  return (
    <div className="menu-con">
      <div className="choice">
        <p className="card">Dinner</p>
        <p className="card">Brunch</p>
        <p className="card">Desert</p>
        <p className="card">Cooktail</p>
      </div>
      <div className="menu">
        <div className="menu-border first">
          <div className="content"></div>
        </div>
      </div>
    </div>
  );
};

export default Menuchoice;
