import React from "react";
import "./Footer.css";

const Footer = () => {
  return (
    <div className="footer">
      <div className="ft-info ft-item">
        <div className="ft-title">Info</div>
        <div className="address">
          Tầng 13-15, Toà B, Khách Sạn Fraser Suites Hanoi, 51 Xuân Diệu, Tây Hồ
        </div>
        <div className="email">contactus@mizumiwestlake.com</div>
        <div className="phone">033 984 1805</div>
      </div>
      <div className="ft-open ft-item">
        <div className="ft-title">Open Time</div>
        <div>11:00 - 14:00 & 18:00 - 22:00</div>
      </div>
      <div className="ft-social ft-item">
        <div className="ft-title">Social Media</div>
        <div>@Mizumi</div>
        <div className="social-icon"></div>
      </div>
    </div>
  );
};

export default Footer;
