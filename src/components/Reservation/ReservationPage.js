import React from "react";
import "./Reservation.css";

const ReservationPage = (props) => {
  const { showPage, closePage, notification } = props;

  return (
    <div className={`reserve-booking ${showPage ? "show" : ""}`}>
      {showPage && (
        <form className="reserve-form">
          <h3 className="reserve-header">Reservations</h3>
          <div className="reserve-items">
            <div className="item location">
              <label htmlFor="location">Location:</label>
              <select id="location" className="input">
                <option value="Hoang Cau">Hoang Cau, Dong Da, Hanoi</option>
                <option value="Hang Bai">Hang Bai, Hoan Kiem, Hanoi</option>
                <option value="Trung Kinh">Trung Kinh, Cau Giay, Hanoi</option>
              </select>
            </div>
            <div className="item number">
              <label htmlFor="number">Number of people:</label>
              <select id="number" className="input">
                <option value="1">1 Person</option>
                <option value="2">2 People</option>
                <option value="3">3 People</option>
                <option value="4">4 People</option>
                <option value="4+">4+ People</option>
              </select>
            </div>
            <div className="item date">
              <label htmlFor="date">Date:</label>
              <input
                className="input"
                type="date"
                id="date"
                placeholder="Date"
              />
            </div>
            <div className="item time">
              <label htmlFor="time">Time:</label>
              <input className="input" type="time" placeholder="Time" />
            </div>
          </div>
          <button className="reserve-submit" onClick={notification}>
            Book a Table
          </button>
        </form>
      )}
      <span className="close-form" onClick={closePage}>
        x
      </span>
    </div>
  );
};

export default ReservationPage;
