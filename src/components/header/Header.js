import React from "react";
import "./Header.css";
import ScrollEvent from "../../function/ScrollEvent";

const Header = (props) => {
  const { showReversePage } = props;
  return (
    <div className={`header ${ScrollEvent(0) ? "bg-show" : ""}`}>
      <div className="header-left">Sona</div>
      <div className="header-right">
        <div>Menu</div>
        <div>Promo</div>
        <div>About Us</div>
        <div onClick={showReversePage}>Reservation</div>
      </div>
    </div>
  );
};

export default Header;
