let PromoList = [
  {
    id: 1,
    imgURL:
      "https://natashaskitchen.com/wp-content/uploads/2020/03/Pan-Seared-Steak-4-500x375.jpg",
    title: "Make yourself at home with 10% discount",
    des: [
      {
        text: "Surprise your loved one or a friend with the perfect present — a meal at Essence Restaurant with a lot of joy and promotions? Currently, the restaurant is having a ",
        bold: false,
      },
      {
        text: "10% Food Promotion",
        bold: true,
      },
      {
        text: " for in-house guests. So why not try to spend an evening at our restaurant and experience the meaning of life?",
        bold: false,
      },
    ],
    expirationDate: "2023-03-02",
  },
  {
    id: 2,
    imgURL:
      "https://natashaskitchen.com/wp-content/uploads/2020/03/Pan-Seared-Steak-4-500x375.jpg",
    title: "Make yourself at home with 10% discount",
    des: [
      {
        text: "Surprise your loved one or a friend with the perfect present — a meal at Essence Restaurant with a lot of joy and promotions? Currently, the restaurant is having a ",
        bold: false,
      },
      {
        text: "10% Food Promotion",
        bold: true,
      },
      {
        text: " for in-house guests. So why not try to spend an evening at our restaurant and experience the meaning of life?",
        bold: false,
      },
    ],
    expirationDate: "2023-03-03",
  },
  {
    id: 3,
    imgURL:
      "https://natashaskitchen.com/wp-content/uploads/2020/03/Pan-Seared-Steak-4-500x375.jpg",
    title: "Make yourself at home with 10% discount",
    des: [
      {
        text: "Surprise your loved one or a friend with the perfect present — a meal at Essence Restaurant with a lot of joy and promotions? Currently, the restaurant is having a ",
        bold: false,
      },
      {
        text: "10% Food Promotion",
        bold: true,
      },
      {
        text: " for in-house guests. So why not try to spend an evening at our restaurant and experience the meaning of life?",
        bold: false,
      },
    ],
    expirationDate: "2023-03-05",
  },
  {
    id: 4,
    imgURL:
      "https://natashaskitchen.com/wp-content/uploads/2020/03/Pan-Seared-Steak-4-500x375.jpg",
    title: "Make yourself at home with 10% discount",
    des: [
      {
        text: "Surprise your loved one or a friend with the perfect present — a meal at Essence Restaurant with a lot of joy and promotions? Currently, the restaurant is having a ",
        bold: false,
      },
      {
        text: "10% Food Promotion",
        bold: true,
      },
      {
        text: " for in-house guests. So why not try to spend an evening at our restaurant and experience the meaning of life?",
        bold: false,
      },
    ],
    expirationDate: "2023-03-08",
  },
];

export default PromoList;
